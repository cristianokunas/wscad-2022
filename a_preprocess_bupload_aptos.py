import argparse
import csv
import sys
import time
from shutil import rmtree
import json
from glob import glob
from os import makedirs, rename, listdir, remove
from os.path import join, basename, exists, isfile
from lib.preprocess import resize_and_center_fundus
import concurrent.futures
from google.cloud import storage

import pyrebase


parser = argparse.ArgumentParser(description='Preprocess Aptos data set and upload.')
parser.add_argument("-d", "--data_dir", help="Directory where Aptos resides.",
                    default="data/aptos/train_images")
parser.add_argument("-u","--path_upload",
                    help="path to the folder of files to be uploaded.",
                    default="data/aptos/bin2")
parser.add_argument("-b", "--bucket_folder",
                    help="bucket where files will be sent.",
                    default="drPipeline")

args = parser.parse_args()
data_dir = str(args.data_dir)
path_upload = str(args.path_upload)
bucketFolder = str(args.bucket_folder)


# config to connect storage with firebase
config = json.load(open('./config/config.json'))
bucketName = config["storageBucket"]

firebase = pyrebase.initialize_app(config)
storage = firebase.storage()

# labels = grades_path

# Create directories for output.
output_dir = join(data_dir, 'bin2')
if exists(output_dir):
    rmtree(output_dir)
makedirs(output_dir)

# Create a tmp directory for saving temporary preprocessing files.
tmp_path = join(data_dir, 'tmp')
if exists(tmp_path):
    rmtree(tmp_path)
makedirs(tmp_path)

failed_images = []
files = [f for f in listdir(data_dir) if isfile(join(data_dir, f))]

# with open(labels, 'r') as f:
#     reader = csv.reader(f, delimiter=',')
#     next(reader)


def process_images(file):
    # for file in files:
    basename = file.split(".")[0]
    im_paths = glob(join(data_dir, "{}*".format(basename)))

    # Find contour of eye fundus in image, and scale
    #  diameter of fundus to 299 pixels and crop the edges.
    res = resize_and_center_fundus(save_path=tmp_path,
                                    image_paths=im_paths,
                                    diameter=299, verbosity=0)

    if res != 1:
        failed_images.append(basename)
        # continue

    new_filename = "{0}.jpg".format(basename)

    rename(join(tmp_path, new_filename),
        join(output_dir, new_filename))
    
    # Status message.
    msg = "\r- Preprocessing image: {0:>7}".format(new_filename)
    sys.stdout.write(msg)
    sys.stdout.flush()

def upload_files(file):
    localFile = join(path_upload, file)
    blob = storage.bucket.blob(join(bucketFolder, file))
    blob.upload_from_filename(localFile)
    # remove(join(path_upload, file))


tStart = time.time()

with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
    print("\r- Preprocessing...")
    executor.map(process_images, files)

files_up = [f for f in listdir(path_upload) if isfile(join(path_upload, f))]

with concurrent.futures.ProcessPoolExecutor(max_workers=5) as excutor:
    print("\n- Uploading...")
    excutor.map(upload_files, files_up)

tElapsed = round(time.time() - tStart, 1)
print('\n Time (sec) to preprocess and upload files: ', tElapsed)

# Clean tmp folder.
rmtree(tmp_path)

# print("\n Could not preprocess {} images.".format(len(failed_images)))
# print(", ".join(failed_images))
