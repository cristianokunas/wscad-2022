#!/bin/bash
# Preprocess script for the Aptos data set.

# Assumes that the data set resides in ./data/aptos.

aptos_dir="./data/aptos"
default_pool_dir="$aptos_dir"
# aptos_path="./data/aptos2019-blindness-detection.zip"
grades_path="./vendor/aptos/aptos_grades.csv"
default_output_dir="$aptos_dir/bin2"
default_bucket_folder="drPipeline"
default_path_upload="$aptos_dir/bin2"
default_path_download="$aptos_dir"

print_usage()
{
  echo ""
  echo "Extracting, preprocessing and upload to cloud script for Aptos."
  echo ""
  echo "Optional parameters: --output_dir, --pool"
  echo ""
  echo "--pool              Path to pool folder (default: $default_pool_dir)"
  echo "--output_dir        Path to output directory (default: $default_output_dir)"
  echo "--bucket_folder     Bucket where files will be send (default: $default_bucket_folder)"
  echo "--path_upload       Path to the folder of files to be uploaded (default: $default_path_upload)"
  echo "--path_download     Path to the folder where the files will de downloaded (default: $default_path_download)"
  echo "--parallel          Define function to preprocess and upload images in parallel. (default: false)"
  exit 1
}

check_parameters()
{
  if [ "$1" -ge 7 ]; then
    echo "Illegal number of parameters".
    exit 1
  fi
  if [ "$1" -ge 1 ]; then
    for param in $2; do
      if [ $(echo "$3" | grep -c -- "$param") -eq 0 ]; then
        echo "Unknown parameter $param."
        exit 1
      fi
    done
  fi
  return 0
}

if echo "$@" | grep -c -- "-h" >/dev/null; then
  print_usage
fi

strip_params=$(echo "$@" | sed "s/--\([a-z_]\+\)\(=\([^ ]\+\)\)\?/\1/g")
check_parameters "$#" "$strip_params" "parallel output pool bucket_folder path_upload path_download todown"

# Get upload directory from parameters.
path_upload=$(echo "$@" | sed "s/.*--path_upload=\([^ ]\+\).*/\1/")

# Check if upload directory is valid.
if ! [[ "$path_upload" =~ ^[^-]+$ ]]; then
  path_upload=$default_path_upload
fi

# Get download directory from parameters.
path_download=$(echo "$@" | sed "s/.*--path_download=\([^ ]\+\).*/\1/")

# Check if download directory is valid.
if ! [[ "$path_download" =~ ^[^-]+$ ]]; then
  path_download=$default_path_download
fi

# Get bucket folder from parameters.
bucket_folder=$(echo "$@" | sed "s/.*--bucket_folder=\([^ ]\+\).*/\1/")

# Check if pool directory is valid.
if ! [[ "$bucket_folder" =~ ^[^-]+$ ]]; then
  bucket_folder=$default_bucket_folder
fi

# Get pool directory from parameters.
pool_dir=$(echo "$@" | sed "s/.*--pool=\([^ ]\+\).*/\1/")

# Check if pool directory is valid.
if ! [[ "$pool_dir" =~ ^[^-]+$ ]]; then
  pool_dir=$default_pool_dir
fi

# Get output directory from parameters.
output_dir=$(echo "$@" | sed "s/.*--output=\([^ ]\+\).*/\1/g")

# Check if output directory is valid.
if ! [[ "$output_dir" =~ ^[^-]+$ ]]; then
  output_dir=$default_output_dir
fi

# # Get output directory from parameters.
# todown=$(echo "$@" | sed "s/.*--output=\([^ ]\+\).*/\1/g")

# # Check if output directory is valid.
# if ! [[ "$todown" =~ ^[^-]+$ ]]; then
#   todown=$default_todown
# fi

if ls "$output_dir" >/dev/null 2>&1; then
  echo "Dataset is already located in $output_dir."
  echo "Specify another output directory with the --output flag."
  exit 1
fi

# aptos_size=$(ls -s $aptos_path 2>/dev/null | cut -d " " -f1)

count_files=$(ls $pool_dir 2>/dev/null | wc -l)

# Copying labels file from vendor to data directory.
# cp "$grades_path" "$pool_dir/labels.csv"

# Preprocess the data set and categorize the images by labels into
#  subdirectories.
if echo "$@" | grep -c -- "--todown" >/dev/null; then
  # TODO download code
  echo "Download function was defined!"
  # exit 1
else
  if echo "$@" | grep -c -- "--parallel" >/dev/null; then
    echo "Pre-process and upload images in parallel simultaneously."
    python3 preprocess_upload_aptos.py --data_dir="$pool_dir" --path_upload="$path_upload" --bucket_folder="$bucket_folder" || exit 1
  else
    echo "Parallel pre-process all and then parallel upload all."
    python3 a_preprocess_bupload_aptos.py --data_dir="$pool_dir" --path_upload="$path_upload" --bucket_folder="$bucket_folder" || exit 1
  fi
fi

# Images into path_upload
echo "Finding images..."
k=$(find "$path_upload" -iname "*.jpg" | wc -l)
echo "Found $k images in $path_upload."

# find "$pool_dir/"[0-4] -iname "*.jpg" -exec mv {} "$output_dir/." \;

# echo "Moving validation tfrecords to separate folder."
# find "$output_dir/train" -name "validation*.tfrecord" -exec mv {} "$output_dir/validation/." \;

echo "Cleaning up..."
# rm -r "$aptos_path" #remove zip file
# rm -r "$aptos_dir/train_images" #remove all full-images extracted
# rm -r "$aptos_dir/*.csv" #remove all csv files
# rm -r "$aptos_dir/"[0-4]

echo "Done!"
exit