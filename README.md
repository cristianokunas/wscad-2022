# Computação de Borda versus Computação em Nuvem: Impacto do Pré-processamento de Imagens de Retinas

Cristiano A. Künas<sup>1</sup>, Dayla R. Pinto<sup>1</sup>, Philippe O. A. Navaux<sup>1</sup> and Lisandro Z. Granville<sup>1</sup>

> <sup>1</sup> Informatics Institute, Federal University of Rio Grande do Sul (UFRGS), Porto Alegre, Brazil

Este repositório contém códigos-fonte do nosso artigo: 

https://cristianokunas.gitlab.io/wscad-2022
